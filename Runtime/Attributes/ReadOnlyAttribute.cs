﻿using System;

// ReSharper disable once CheckNamespace
namespace InnovationLab
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ReadOnlyAttribute : Attribute
    {
    }
}