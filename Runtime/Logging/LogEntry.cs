﻿
using UnityEngine;

namespace InnovationLab.Logging
{
    public class LogEntry
    {
        static readonly Color DefaultColor = UnityEngine.Color.red;
        static readonly float DefaultDuration = 10;

        internal readonly string text;
        internal Color color;
        internal float duration;

        internal LogEntry(string text) {
            this.text = text;
            this.color = DefaultColor;
            this.duration = DefaultDuration;
        }

        public LogEntry Color(Color color) {
            this.color = color;
            return this;
        }

        public LogEntry Duration(float duration) {
            this.duration = duration;
            return this;
        }
    }
}