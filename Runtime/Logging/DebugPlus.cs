﻿// Adapted from https://github.com/benoit-dumas/DebugPlus
// MIT license

namespace InnovationLab.Logging {

public static class DebugPlus {
    /// <summary>
    /// Draws text directly on the screen.
    /// Returns the <see cref="DebugPlusNS.LogEntry"/> to change drawing color and duration.
    /// </summary>
    /// <param name="text"></param>
    /// <returns>The fluent object to add parameters.</returns>
    public static LogEntry LogOnScreen(string text) {
        var res = new LogEntry(text);
        LogDrawer.Log(res);
        return res;
    }
}

}