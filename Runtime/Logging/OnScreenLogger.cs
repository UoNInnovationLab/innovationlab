using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace InnovationLab.Logging
{
    /// <summary>
    /// Push all Debug.Log message to an onscreen log.
    /// This is off by default, turn on using <c>OnScreenLogger.SetOnScreenLogger(true)</c>.
    /// </summary>
    public static class OnScreenLogger
    {
        public static void SetOnScreenLogger(bool useOnScreenLog)
        {
            if (useOnScreenLog)
            {
                Application.logMessageReceived += Application_OnLogMessageReceived;
            }
            else
            {
                Application.logMessageReceived -= Application_OnLogMessageReceived;
            }
        }

        static void Application_OnLogMessageReceived(string message, string stacktrace, LogType logType)
        {
            DebugPlus.LogOnScreen(message).Color(LogTypeToColor(logType));
        }

        static Color LogTypeToColor(LogType logType)
        {
            switch (logType)
            {
                case LogType.Assert:
                    return Color.red;
                case LogType.Error:
                    return Color.red;
                case LogType.Exception:
                    return Color.red;
                case LogType.Log:
                    return Color.white;
                case LogType.Warning:
                    return Color.yellow;
            }

            // Should not get here
            return Color.green;
        }
    }
}