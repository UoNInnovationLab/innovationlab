﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace InnovationLab.Logging
{
    public class LogDrawer : SingletonBehavior<LogDrawer>
    {
        const int LogCapacity = 30;

        readonly Queue<LogEntry> m_LogEntries = new Queue<LogEntry>();
        readonly Queue<LogEntry> m_NewLogEntries = new Queue<LogEntry>();
        List<Text> m_Texts;

        public static void Log(LogEntry logEntry)
        {
            if (I != null) I.m_NewLogEntries.Enqueue(logEntry);
        }

        void Start()
        {
            GameObject cgo = new GameObject("DebugPlusCanvas");

            // canvas
            var canvas = cgo.AddComponent<Canvas>();
            canvas.sortingOrder = 100;
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;

            // vertical layout
            var verticalLayoutGroup = new GameObject("Log", typeof(VerticalLayoutGroup));
            verticalLayoutGroup.transform.SetParent(cgo.transform);

            var vlg = verticalLayoutGroup.GetComponent<VerticalLayoutGroup>();
            vlg.childForceExpandHeight = false;
            vlg.childForceExpandWidth = false;
            vlg.childControlHeight = false;

            var vlRect = verticalLayoutGroup.GetComponent<RectTransform>();
            vlRect.anchorMin = new Vector2(0.05f, 0.3f);
            vlRect.anchorMax = new Vector2(0.7f, 0.95f);
            vlRect.anchoredPosition = Vector2.zero;
            vlRect.sizeDelta = Vector2.zero;

            // texts
            I.m_Texts = new List<Text>();
            for (int i = 0; i < LogCapacity; i++)
            {
                var go = new GameObject("Log entry", typeof(Text));
                go.transform.SetParent(verticalLayoutGroup.transform);
                go.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 17);
                //go.GetComponent<RectTransform>().ForceUpdateRectTransforms();
                var t = go.GetComponent<Text>();
                t.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
                I.m_Texts.Add(t);
            }
        }
        
        void Update()
        {
            // add new entries
            while (m_NewLogEntries.Count > 0)
            {
                var le = m_NewLogEntries.Dequeue();
                m_LogEntries.Enqueue(le);
            }

            foreach (var le in m_LogEntries)
            {
                le.duration -= Time.deltaTime;
            }

            // Remove entries that don't fit
            while (m_LogEntries.Count > LogCapacity) m_LogEntries.Dequeue();
            
            // Remove entries that are too old. Assume they are in chronological order.
            while (m_LogEntries.Count > 0 && m_LogEntries.Peek().duration <= 0) m_LogEntries.Dequeue();

            if (m_Texts != null)
            {
                // Set the text values
                int index = 0;
                foreach (var le in m_LogEntries)
                {
                    var t = m_Texts[index++];
                    t.gameObject.SetActive(true);
                    t.text = le.text;
                    t.color = le.color;
                }
                
                // Deactivate unused texts
                for (int i = m_LogEntries.Count; i < LogCapacity; i++)
                {
                    var t = m_Texts[i];
                    t.gameObject.SetActive(false);
                }
            }
        }
    }
}