﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace InnovationLab
{

    public class FPSCounter : MonoBehaviour
    {
        public Text m_Text;
        public GameObject m_toggleGamobject;
        public KeyCode m_toggleKey;

        const float fpsMeasurePeriod = 0.5f;
        private int m_FpsAccumulator = 0;
        private float m_FpsNextPeriod = 0;
        private int m_CurrentFps;
        const string display = "{0} FPS";
        

        private void Start()
        {
            m_FpsNextPeriod = Time.realtimeSinceStartup + fpsMeasurePeriod;
        }


        private void Update()
        {
            if (Input.GetKeyDown(m_toggleKey))
            {
                m_toggleGamobject.SetActive(!m_toggleGamobject.activeSelf);    
            }

            // measure average frames per second
            m_FpsAccumulator++;
            if (Time.realtimeSinceStartup > m_FpsNextPeriod)
            {
                m_CurrentFps = (int)(m_FpsAccumulator / fpsMeasurePeriod);
                m_FpsAccumulator = 0;
                m_FpsNextPeriod += fpsMeasurePeriod;
                m_Text.text = string.Format(display, m_CurrentFps);
            }
        }
    }
}

