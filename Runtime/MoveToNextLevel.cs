﻿using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MoveToNextLevel : MonoBehaviour
{
    [Header("Scene To Load")]
    public string nextSceneName;
    private Scene nextScene;
    public bool delayLoadUntilCall;

    [Header("Time Based")]
    public bool useTime;
    public float delayToAutoMove = 15f;

    [Header("UI for time")]
    public bool showTimer;
    public Text timerText;

    [Header("NextButton")]
    public bool showNextButton;
    public GameObject nextButton;
    public float buttonShowDelay;

    private AsyncOperation _loadingOperation;
    private float _startTime;

    private bool _triedMove;

	// Use this for initialization
	void Start ()
	{
	    _startTime = Time.time;

        if (delayLoadUntilCall)
	        return;

        _loadingOperation = SceneManager.LoadSceneAsync(nextSceneName);
        
	    _loadingOperation.allowSceneActivation = false;

        if(timerText != null)
            timerText.gameObject.SetActive( showTimer );

        if(nextButton != null)
            nextButton.SetActive( Math.Abs(buttonShowDelay) < 0.0001f );
	}
    
    void Update ()
    {
        if (_triedMove) return;

        if(useTime && Time.time - _startTime < delayToAutoMove)
        { 
            if(showTimer)
                timerText.text = Convert.ToInt32((delayToAutoMove + (_startTime - Time.time)) * 0.6666f).ToString(CultureInfo.InvariantCulture);
        }
        else if(useTime)
        {
            _triedMove = true;
            _loadingOperation.allowSceneActivation = true;
        }

        if (showNextButton && Time.time - _startTime > buttonShowDelay && !nextButton.activeSelf)
           nextButton.SetActive(true);
        
        
    }

    public void NextLevel()
    {
        _triedMove = true;
        if(delayLoadUntilCall)
            _loadingOperation = SceneManager.LoadSceneAsync(nextSceneName);
        _loadingOperation.allowSceneActivation = true;

  
    }

    public void ExitApplication()
    {
        #if UNITY_EDITOR
            // Application.Quit() does not work in the editor so...
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

}
