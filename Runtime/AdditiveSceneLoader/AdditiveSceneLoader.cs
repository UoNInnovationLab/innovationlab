﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace InnovationLab
{
    public class AdditiveSceneLoader : MonoBehaviour
    {
        public string[] GameScenes;
        public string[] ErrorScenes;
        public int RemoteSettingsWaitTimeout = 3;

        public bool UseVersionLock = false;

        public List<GameObject> thingsToTurnOffOnLoad;

        private int _remoteVersion = 100;
        private RemoteSettings.UpdatedEventHandler _remoteSettingsHandler;

        

        void Start()
        {
            _remoteSettingsHandler = CheckVersionAndLoadScenes;
            if (UseVersionLock)
            {
                RemoteSettings.Updated += _remoteSettingsHandler;
                Invoke("RemoteSettingsTimeout", RemoteSettingsWaitTimeout);
            }
            else
            {
                LoadGameScenes();
            }
        }

        private void RemoteSettingsTimeout()
        {
            // Timed out waiting for remote settings. This may happen if RemoteSettings.Updated is never called
            // Ex: the app is started for the first time and there's no internet. In that case load the gamy anyway
            RemoteSettings.Updated -= _remoteSettingsHandler;
            LoadGameScenes();
        }


        void CheckVersionAndLoadScenes()
        {
            //we've got response from RemoteSettings so we cancel tehr timeout coroutine
            CancelInvoke("RemoteSettingsTimeout");

            _remoteVersion = RemoteSettings.GetInt("MinVersionAllowed", _remoteVersion);

            UnityEngine.Debug.Log("Remote MinAllowedVersion: " + _remoteVersion);
            if (AppVersion.APP_VERSION >= _remoteVersion)
                LoadGameScenes();
            else
                LoadErrorScenes();
        }

        private void LoadErrorScenes()
        {
            if (thingsToTurnOffOnLoad != null && thingsToTurnOffOnLoad.Count != 0)
                thingsToTurnOffOnLoad.ForEach(Destroy);

            //Stop waiting for timeout
            CancelInvoke();
            HashSet<string> loadedScenes = new HashSet<string>();

            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                loadedScenes.Add(SceneManager.GetSceneAt(i).name);
            }

            foreach (var scene in ErrorScenes)
            {
                if (!loadedScenes.Contains(scene))
                {
                    SceneManager.LoadScene(scene, LoadSceneMode.Additive);
                }
            }
        }


        private void LoadGameScenes()
        {
            if (thingsToTurnOffOnLoad != null && thingsToTurnOffOnLoad.Count != 0)
                thingsToTurnOffOnLoad.ForEach(Destroy);

            HashSet<string> loadedScenes = new HashSet<string>();

            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                loadedScenes.Add(SceneManager.GetSceneAt(i).name);
            }

            foreach (var scene in GameScenes)
            {
                if (!loadedScenes.Contains(scene))
                {
                    SceneManager.LoadScene(scene, LoadSceneMode.Additive);
                }
            }
        }
    }
}



