﻿using System.Diagnostics;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace InnovationLab
{
    public static class Check
    {
        [Conditional("UNITY_EDITOR")]
        public static void IsNotNull(Object objectToCheck, string paramName, Object context=null )
        {
            if (objectToCheck == null)
            {
                UnityEngine.Debug.LogError($"{paramName.ToDisplayName()} is null. Please assign it in the editor.", context);
            }
        }
        
        [Conditional("UNITY_EDITOR")]
        public static void IsNotNull(string stringToCheck, string paramName, Object context=null )
        {
            if (string.IsNullOrEmpty(stringToCheck))
            {
                UnityEngine.Debug.LogError($"{paramName.ToDisplayName()} is null. Please assign it in the editor.", context);
            }
        }
        
        [Conditional("UNITY_EDITOR")]
        public static void IsEqual(int val1, int val2, Object context=null )
        {
            if (val1 != val2)
            {
                UnityEngine.Debug.LogError($"{val1} doesn't equal value {val2}.", context);
            }
        }
    }
}