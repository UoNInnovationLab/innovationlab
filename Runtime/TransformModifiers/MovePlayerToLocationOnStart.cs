﻿using UnityEngine;

public class MovePlayerToLocationOnStart : MonoBehaviour
{
    public Vector3 initialPosition;
    public string playerRootName;

	void Start ()
	{
	    GameObject.Find(playerRootName).transform.position = initialPosition;
	}
}
