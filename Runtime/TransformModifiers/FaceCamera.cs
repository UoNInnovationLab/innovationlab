﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
//-------------------------------------------------------------------------
[CustomEditor(typeof(FaceCamera))]
[CanEditMultipleObjects]
public class FaceCameraEditor : Editor
{
    protected SerializedProperty alternateForward;
    protected SerializedProperty axis;
    protected SerializedProperty useAxis;
    protected SerializedProperty useLocalAxis;
    protected SerializedProperty dampFactor;

    protected SerializedProperty useCustomLookAtPoint;
    protected SerializedProperty customLookAtTransform;

    protected SerializedProperty matchCameraHeight;
    protected SerializedProperty maxVerticalDrift;
    protected SerializedProperty startingHeight;
    protected SerializedProperty maxMovement;

    protected virtual void OnEnable()
    {
        alternateForward = serializedObject.FindProperty("alternateForward");
        axis = serializedObject.FindProperty("axis");
        useAxis = serializedObject.FindProperty("useAxis");
        useLocalAxis = serializedObject.FindProperty("useLocalAxis");
        dampFactor = serializedObject.FindProperty("dampFactor");

        useCustomLookAtPoint = serializedObject.FindProperty("useCustomLookAtPoint");
        customLookAtTransform = serializedObject.FindProperty("customLookAtTransform");

        matchCameraHeight = serializedObject.FindProperty("matchCameraHeight");
        maxVerticalDrift = serializedObject.FindProperty("maxVerticalDrift");
        maxMovement = serializedObject.FindProperty("maxMovement");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.Separator();
        if (!serializedObject.isEditingMultipleObjects)
            EditorGUILayout.PropertyField(alternateForward, new GUIContent("Use alternate forward", "Position faces target, others use targets transform"));

        EditorGUILayout.Separator();
        EditorGUILayout.PropertyField(useAxis, new GUIContent("Use Axis", "Locks rotation to an axis"));
        if (useAxis.boolValue)
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.PropertyField(useLocalAxis, new GUIContent("Use root local axis"));
            if (!serializedObject.isEditingMultipleObjects)
                EditorGUILayout.PropertyField(axis, new GUIContent("Axis"));
            EditorGUILayout.EndVertical();
        }
        EditorGUILayout.Separator();
        EditorGUILayout.PropertyField(dampFactor, new GUIContent("Damping factor", "Damp rotation speed"));

        EditorGUILayout.Separator();
        EditorGUILayout.PropertyField(useCustomLookAtPoint, new GUIContent("Use custom LookAt point"));
        EditorGUILayout.PropertyField(customLookAtTransform, new GUIContent("Custom LookAt transform", "Default is main camera"));

        EditorGUILayout.Separator();
        EditorGUILayout.PropertyField(matchCameraHeight, new GUIContent("Match camera height", "Make object float to camera height"));
        EditorGUILayout.PropertyField(maxVerticalDrift, new GUIContent("Maximum vertical drift"));
        EditorGUILayout.PropertyField(maxMovement, new GUIContent("Maximum movement"));
        
        EditorGUILayout.Separator();
        serializedObject.ApplyModifiedProperties();
    }
}
#endif

public class FaceCamera : MonoBehaviour
{ 
    public enum Axis { X, Y, Z }
    public enum Forward {Position, Forward, Right, Up }

    public Forward alternateForward;

    public float dampFactor = 0.9f;

    public bool useAxis;
    public bool useLocalAxis;
    public Axis axis = Axis.X;
    
    public bool useCustomLookAtPoint;
    public Transform customLookAtTransform;

    public bool matchCameraHeight;
    public float maxVerticalDrift = 0.5f;
    private float _startingHeight;
    public float maxMovement;
    
    private void Start ()
    {
        _startingHeight = transform.position.y;
	}
    
	void Update () {
        RotateToCameraWithSmoothing();
        if(matchCameraHeight)
            MatchCameraHeight();
	}


    private void RotateToCameraWithSmoothing()
    {
        if (useAxis && useLocalAxis)
                RotateLocal();

        Vector3 direction = Vector3.zero;

        var transformToUse = useCustomLookAtPoint ? customLookAtTransform : Camera.main.transform;

        switch (alternateForward)
        {
            case Forward.Position:
                direction = transform.position - transformToUse.position;
                direction = direction.normalized;
                break;
            case Forward.Forward:
                direction = transformToUse.forward;
                break;
            case Forward.Right:
                direction = transformToUse.right;
                break;
            case Forward.Up:
                direction = Vector3.up;
                break;
        }
        
        Quaternion targetRotation = Quaternion.LookRotation(direction);

        if (useAxis)
        {
            Vector3 tmpLocalEulerAngles = targetRotation.eulerAngles;
            
            switch (axis)
            {
                case Axis.X:
                    tmpLocalEulerAngles = tmpLocalEulerAngles.Mul(Vector3.right).normalized;
                    break;
                case Axis.Y:
                    tmpLocalEulerAngles = tmpLocalEulerAngles.Mul( Vector3.up ).normalized;
                    break;
                case Axis.Z:
                    tmpLocalEulerAngles = tmpLocalEulerAngles.Mul( Vector3.forward ).normalized;
                    break;
            }
            
            targetRotation = Quaternion.Euler(tmpLocalEulerAngles);
        }
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, dampFactor * Time.deltaTime);
    }

    private void RotateLocal()
    {
        var direction = Vector3.zero;

        var transformToUse = useCustomLookAtPoint ? customLookAtTransform : Camera.main.transform;

        switch (alternateForward)
        {
            case Forward.Position:
                direction = transform.position - transformToUse.position; 
                direction = direction.normalized;
                break;
            case Forward.Forward:
                direction = transformToUse.forward;
                break;
            case Forward.Right:
                direction = transformToUse.right;
                break;
            case Forward.Up:
                direction = Vector3.up;
                break;
        }               
        

        var localAxis = Vector3.zero;

        if (useLocalAxis)
            localAxis = axis == Axis.X ? transform.root.right : (axis == Axis.Y ? transform.root.up : transform.root.forward);
        else
            localAxis = axis == Axis.X ? Vector3.right : (axis == Axis.Y ? Vector3.up : Vector3.forward);
        
        var projected = Vector3.ProjectOnPlane(direction, localAxis).normalized;

        if (alternateForward == Forward.Position)
            transform.LookAt(transform.position + projected, localAxis * Vector3.Dot(transformToUse.forward, transform.forward)) ;
        else
            transform.LookAt(transform.position + projected, localAxis);
        
    }

    private void MatchCameraHeight()
    {
        var yPos = Mathf.Clamp( Camera.main.transform.position.y, _startingHeight - maxMovement, _startingHeight + maxMovement);
        var newPos = new Vector3( transform.position.x, yPos, transform.position.z);
        transform.position = Vector3.Slerp(transform.position, newPos, dampFactor * Time.deltaTime);
    }
}
