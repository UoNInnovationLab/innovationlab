﻿using UnityEngine;

public class MoveMeToNewParent : MonoBehaviour
{
    public enum MoveTime { Awake, Start, Update, Destroy, CollisionEnter , CollisionExit, TriggerEnter, TriggerExit }

    public MoveTime WhenToMove;

    public Transform newParent;

    void Awake()
    {
        if(WhenToMove == MoveTime.Awake)
            transform.parent = newParent;
    }

    void Start()
    {
        if (WhenToMove == MoveTime.Start)
            transform.parent = newParent;
    }

    void Update()
    {
        if (WhenToMove == MoveTime.Update)
            transform.parent = newParent;
    }

    void OnDestroy()
    {
        if (WhenToMove == MoveTime.Destroy)
            transform.parent = newParent;
    }

    void OnCollisionEnter(Collision col)
    {
        if (WhenToMove == MoveTime.CollisionEnter)
            transform.parent = newParent;
    }
    void OnCollisionExit(Collision col)
    {
        if (WhenToMove == MoveTime.CollisionExit)
            transform.parent = newParent;
    }

    void OnTriggerEnter(Collider col)
    {
        if (WhenToMove == MoveTime.TriggerEnter)
            transform.parent = newParent;
    }

    void OnTriggerExit(Collider col)
    {
        if (WhenToMove == MoveTime.TriggerExit)
            transform.parent = newParent;
    }


}
