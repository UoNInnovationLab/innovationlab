﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public static class RandomExtensions
{
    public static float GetWeightedRange(this Random t, float minRand, float maxRand, float minRange, float maxRange, float percentage)
    {
        return Random.Range(0f, 100f) < percentage ? Random.Range(minRange, maxRange) : Random.Range(minRand, maxRand);
    }
}
