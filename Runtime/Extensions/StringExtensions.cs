﻿using System;
using System.Globalization;
using UnityEngine;

public static class StringExtensions
{
    static readonly char[] TrimChars = {' ', '\r', '\n', '\t', '(', ')'};

    public static string Capitalize(this string target)
    {
        if (string.IsNullOrEmpty(target)) return target;
        return target.Substring(0, 1).ToUpper() + target.Substring(1);
    }

    public static string TrimStart(this string target, string prefix)
    {
        if (string.IsNullOrEmpty(target)) return target;
        return target.StartsWith(prefix, false, CultureInfo.CurrentCulture) ? target.Substring(prefix.Length) : target;
    }

    /// <summary>
    /// Returns the index of the first uppercase character in the string.
    /// </summary>
    public static int IndexOfUppercaseLetter(this string target, int startIndex = 0)
    {
        if (string.IsNullOrEmpty(target)) return -1;
        if (startIndex >= target.Length) return -1;

        for (int index = startIndex; index < target.Length; index++)
        {
            if (Char.IsUpper(target[index]))
            {
                return index;
            }
        }

        return -1;
    }

    /// <summary>
    /// Returns the index of the first lowercase character in the string.
    /// </summary>
    public static int IndexOfLowercaseLetter(this string target, int startIndex = 0)
    {
        if (string.IsNullOrEmpty(target)) return -1;
        if (startIndex >= target.Length) return -1;

        for (int index = startIndex; index < target.Length; index++)
        {
            if (Char.IsLower(target[index]))
            {
                return index;
            }
        }

        return -1;
    }

    public static int IndexOfUpperLowerPair(this string target, int startIndex = 0)
    {
        if (string.IsNullOrEmpty(target)) return -1;
        if (target.Length == 1) return -1;
        if (startIndex >= target.Length) return -1;

        int index = startIndex;
        int safety = 10;

        do
        {
            index = target.IndexOfUppercaseLetter(index);
            if (index >= target.Length - 1) return -1;
            if (index == -1) return -1;
            if (Char.IsLower(target[index + 1])) return index;
            index++;
        } while (index != -1 && safety-- > 0);

        return -1;
    }

    public static string SeparateWords(this string target)
    {
        if (string.IsNullOrEmpty(target)) return target;
        int index = 0;
        int safety = 10;
        do
        {
            index = target.IndexOfUpperLowerPair(index);
            if (index != 0 && index != -1)
            {
                target = target.Insert(index, " ");
                index += 2;
            }

            if (index == 0) index++;
        } while (index != 0 && index != -1 && safety-- > 0);

        return target;
    }
    
    /// <summary>
    /// Convert a variable name to a human readable phrase. Similar to properties in the Unity editor.
    /// </summary>
    /// <param name="variableName"></param>
    /// <returns></returns>
    public static string ToDisplayName(this string variableName)
    {
        return variableName
            .TrimStart("m_")
            .TrimStart("m")
            .TrimStart("_")
            .SeparateWords()
            .Capitalize();
    }

    public static Vector3 ToVector3(this string target)
    {
        target = target.Trim(TrimChars);
        if (string.IsNullOrEmpty(target)) return Vector3.zero;

        string[] szParseString = target.Split(',');

        float x = float.Parse(szParseString[0]);
        float y = float.Parse(szParseString[1]);
        float z = float.Parse(szParseString[2]);

        return new Vector3(x, y, z);
    }
}