﻿using UnityEngine;

public static class Vector3Extensions
{
    public static string ToStringEx(this Vector3 v)
    {
        return $"({v.x:F8}, {v.y:F8}, {v.z:F8})";
    }
    
    //-------------------------------------------------
    public static Vector3 Mul (this Vector3 v, Vector3 v2)
    {
        return new Vector3(v.x * v2.x, v.y * v2.y, v.z * v2.z);
    }

    //-------------------------------------------------
    public static Vector2 ToXZ(this Vector3 v)
    {
        return new Vector2(v.x, v.z);
    }

    //-------------------------------------------------
    public static float Yaw(this Vector3 v)
    {
        float fDist = v.magnitude;

        if (v.z >= 0.0f)
        {
            return Mathf.Acos(v.x / fDist);
        }
        else
        {
            return Mathf.Acos(-v.x / fDist) + Mathf.PI;
        }
    }
    //-------------------------------------------------
    public static Vector3 BezierInterpolate3(this Vector3 v1, Vector3 p0, Vector3 c0, Vector3 p1, float t)
    {
        Vector3 p0c0 = Vector3.Lerp(p0, c0, t);
        Vector3 c0p1 = Vector3.Lerp(c0, p1, t);

        return Vector3.Lerp(p0c0, c0p1, t);
    }


    //-------------------------------------------------
    public static Vector3 BezierInterpolate4( this Vector3 v1, Vector3 p0, Vector3 c0, Vector3 c1, Vector3 p1, float t)
    {
        Vector3 p0c0 = Vector3.Lerp(p0, c0, t);
        Vector3 c0c1 = Vector3.Lerp(c0, c1, t);
        Vector3 c1p1 = Vector3.Lerp(c1, p1, t);

        Vector3 x = Vector3.Lerp(p0c0, c0c1, t);
        Vector3 y = Vector3.Lerp(c0c1, c1p1, t);

        //Debug.DrawRay(p0, Vector3.forward);
        //Debug.DrawRay(c0, Vector3.forward);
        //Debug.DrawRay(c1, Vector3.forward);
        //Debug.DrawRay(p1, Vector3.forward);

        //Gizmos.DrawSphere(p0c0, 0.5F);
        //Gizmos.DrawSphere(c0c1, 0.5F);
        //Gizmos.DrawSphere(c1p1, 0.5F);
        //Gizmos.DrawSphere(x, 0.5F);
        //Gizmos.DrawSphere(y, 0.5F);

        return Vector3.Lerp(x, y, t);
    }

}
