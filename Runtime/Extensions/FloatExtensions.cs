﻿using System;
using UnityEngine;

public static class FloatExtensions 
{
    public static string ToStringEx(this float f)
    {
        return $"{f:F8}";
    }

    public static float RemapNumber(this float num, float low1, float high1, float low2, float high2)
    {
        return low2 + (num - low1) * (high2 - low2) / (high1 - low1);
    }


//-------------------------------------------------
    public static float RemapNumberClamped(this float num, float low1, float high1, float low2, float high2)
    {
        return Mathf.Clamp(RemapNumber(num, low1, high1, low2, high2), Mathf.Min(low2, high2), Mathf.Max(low2, high2));
    }


//-------------------------------------------------
    public static float Approach(this float target, float value, float speed)
    {
        float delta = target - value;
        
        if (delta > speed)
            value += speed;
        else if (delta < -speed)
            value -= speed;
        else
            value = target;

        return value;
    }
    //-------------------------------------------------
    public static float Normalize(this float value, float min, float max)
    {
        float normalizedValue = (value - min) / (max - min);

        return normalizedValue;
    }
    //-------------------------------------------------
    // Truncate floats to the specified # of decimal places when you want easier-to-read numbers without clamping to an int
    //-------------------------------------------------
    public static decimal ToDecimal(this float value, int decimalPlaces = 2)
    {
        return Math.Round((decimal)value, decimalPlaces);
    }


}
