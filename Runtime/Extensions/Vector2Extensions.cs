﻿using UnityEngine;

public static class Vector2Extensions
{
    public static string ToStringEx(this Vector2 v)
    {
        return $"({v.x:F8}, {v.y:F8})";
    }

}
