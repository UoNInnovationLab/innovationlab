﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InnovationLab
{
    public class AppVersion
    {
        // Hardcoded for now
        public static int APP_VERSION = 100;        
    }
}
