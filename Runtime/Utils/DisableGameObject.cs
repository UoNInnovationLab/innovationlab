﻿using UnityEngine;

namespace InnovationLab
{
    /// <summary>
    /// Disable a GameObject either in the player, in the Editor, or both.
    /// This is useful for debug or simulation objects that you don't want to run in a build.
    /// </summary>
    public class DisableGameObject : MonoBehaviour
    {
        enum WhenToDisable
        {
            Never,
            InEditor,
            InPlayer,
            Always
        }

        [SerializeField] WhenToDisable m_WhenToDisable = WhenToDisable.Never;

#if UNITY_EDITOR
        void Awake()
        {
            gameObject.SetActive(m_WhenToDisable != WhenToDisable.InEditor || m_WhenToDisable == WhenToDisable.Always);
        }
#else
    void Awake()
    {
        gameObject.SetActive(m_WhenToDisable!=WhenToDisable.InPlayer||m_WhenToDisable==WhenToDisable.Always);
    }
#endif
    }
}