﻿using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;

namespace InnovationLab.Utils
{
    /// <summary>
    /// In debug builds, this class will log every pointer interaction, submit and select events on a GameObject.
    /// This is useful to debug UI, especially with VR pointer interactions.
    /// </summary>
    public class LogPointerEvents : MonoBehaviour, IPointerDownHandler, IPointerUpHandler,
        IPointerEnterHandler, IPointerExitHandler, ISelectHandler, ISubmitHandler
    {
        public bool m_Mute = true;

        public void OnPointerDown(PointerEventData eventData)
        {
            DoDebugLog($"{name}.OnPointerDown({eventData})", this);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            DoDebugLog($"{name}.OnPointerUp({eventData})", this);
        }
        public void OnPointerEnter(PointerEventData eventData)
        {
            DoDebugLog($"{name}.OnPointerEnter({eventData})", this);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            DoDebugLog($"{name}.OnPointerExit({eventData})", this);
        }

        public void OnSelect(BaseEventData eventData)
        {
            DoDebugLog($"{name}.OnSelect({eventData})", this);
        }

        public void OnSubmit(BaseEventData eventData)
        {
            DoDebugLog($"{name}.OnSubmit({eventData})", this);
        }

        [Conditional("DEBUG")]
        void DoDebugLog(string msg, Object context)
        {
            if (!m_Mute)
                UnityEngine.Debug.Log(msg, context);
        }
    }
}
