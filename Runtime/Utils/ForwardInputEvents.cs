﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace InnovationLab
{
    public class ForwardInputEvents : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler,
        IPointerExitHandler, IPointerClickHandler
    {
        public enum ForwardTo
        {
            Hierarchy,
            Parent,
            Custom
        }

        [SerializeField] ForwardTo m_ForwardTo = default;
        [SerializeField] GameObject m_Receiver = default;

        void OnValidate()
        {
            if (m_ForwardTo == ForwardTo.Parent)
            {
                if (transform.parent != null) m_Receiver = transform.parent.gameObject;
            }
            else if (m_ForwardTo == ForwardTo.Hierarchy)
            {
                if (transform.parent != null) m_Receiver = transform.parent.gameObject;
            }

            if (m_Receiver == null)
            {
                Debug.LogError("Please set the input event receiver.", this);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            ExecutePointerEvent(ExecuteEvents.pointerDownHandler, eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            ExecutePointerEvent(ExecuteEvents.pointerUpHandler, eventData);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            ExecutePointerEvent(ExecuteEvents.pointerEnterHandler, eventData);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            ExecutePointerEvent(ExecuteEvents.pointerExitHandler, eventData);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            ExecutePointerEvent(ExecuteEvents.pointerClickHandler, eventData);
        }

        void ExecutePointerEvent<T>(ExecuteEvents.EventFunction<T> eventFunction, PointerEventData eventData)
            where T : IEventSystemHandler
        {
            if (m_ForwardTo == ForwardTo.Hierarchy)
            {
                ExecuteEvents.ExecuteHierarchy(m_Receiver, eventData, eventFunction);
            }
            else if (m_Receiver != null)
            {
                ExecuteEvents.Execute(m_Receiver, eventData, eventFunction);
            }
        }
    }
}