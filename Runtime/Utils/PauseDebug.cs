﻿using UnityEngine;

namespace InnovationLab.Utils
{
	public class PauseDebug : MonoBehaviour
	{
		public KeyCode pauseKey;
	
		// Update is called once per frame
		void Update ()
		{
			if (Application.isEditor)
			{
				if (Input.GetKeyDown(pauseKey))
				{
					Debug.Break();
				}
			}
		}
	}
}
