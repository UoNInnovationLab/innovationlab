﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace InnovationLab.Utils
{
	public class DistanceLogger : MonoBehaviour
	{
		public KeyCode key;
		public GameObject[] objects;

		private class Dist
		{
			public GameObject go;
			public float distance;
		}

		// Update is called once per frame
		void Update () {
			if (Input.GetKeyDown(key))
			{
				List<Dist> distances = new List<Dist>();

				foreach (var obj in objects)
				{
					float dist = Vector3.Distance(gameObject.transform.position, obj.GetComponent<Renderer>().bounds.center);
					distances.Add(new Dist() {go = obj, distance = dist});
				}

				distances = distances.OrderBy(o => o.distance).ToList();

				foreach (var d in distances)
				{
					UnityEngine.Debug.Log(d.go.transform.name + " " + d.distance);
				}
			}

		}
	}
}
