﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// By default Toggles don't have a color for when they're toggled on.
/// Attach this script to a GameObject with a Toggle component and it will set the Toggle to a color when it is on.
/// Thanks to https://answers.unity.com/questions/1349438/toggle-button-different-color-for-on-and-off-state-1.html
/// </summary>
[RequireComponent(typeof(Toggle))]
public class Toggle_IsOnColor : MonoBehaviour
{
    [Tooltip("The color of the Toggle when it is on")]
    [SerializeField] Color m_OnColor = default;
    [Tooltip("The color of the Toggle when it is on and is highlighted")]
    [SerializeField] Color m_OnHighlightColor = default;
    
    Color m_StartNormalColor = default;
    Color m_StartHighlightColor = default;

    private Toggle m_Toggle;
 
    void OnEnable()
    {
        m_Toggle = GetComponent<Toggle>();
        m_Toggle.onValueChanged.AddListener(OnToggleValueChanged);

        var colors = m_Toggle.colors;
        m_StartNormalColor = colors.normalColor;
        m_StartHighlightColor = colors.highlightedColor;
    }

    void OnDisable()
    {
        m_Toggle.onValueChanged.RemoveListener(OnToggleValueChanged);
    }

    private void OnToggleValueChanged(bool isOn)
    {
        ColorBlock cb = m_Toggle.colors;
        if (isOn)
        {
            cb.normalColor = m_OnColor;
            cb.highlightedColor = m_OnHighlightColor;
        }
        else
        {
            cb.normalColor = m_StartNormalColor;
            cb.highlightedColor = m_StartHighlightColor;
        }
        m_Toggle.colors = cb;
    }
}
