﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Toggle doesn't trigger a selected animation, or any permanent state when it is on.
/// Attach this script to a GameObject with a Toggle component and it will trigger the "Selected" anim.
/// </summary>
/// 
[RequireComponent(typeof(Toggle), typeof(Animator))]
public class Toggle_TriggerSelectionAnimation : MonoBehaviour
{
    public string m_SelectedTrigger = "Selected";
    public string m_UnselectedTrigger = "Unselected";

    Toggle m_Toggle;
    Animator m_Animator;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Toggle = GetComponent<Toggle>();
        m_Toggle.onValueChanged.AddListener(Toggle_OnValueChanged);
    }

    void OnDestroy()
    {
        m_Toggle.onValueChanged.AddListener(Toggle_OnValueChanged);
    }

    void Toggle_OnValueChanged(bool value)
    {
        var trigger = value ? m_SelectedTrigger : m_UnselectedTrigger;
        m_Animator.SetTrigger(trigger);
    }
}
