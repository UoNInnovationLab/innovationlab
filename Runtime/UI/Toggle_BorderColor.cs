﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Attach this script to a GameObject with a Toggle component and it will st the border Image when it's is on.
/// </summary>
[RequireComponent(typeof(Toggle))]
public class Toggle_BorderColor : MonoBehaviour
{
    [SerializeField] Image m_BorderImage = default;
    [SerializeField] Color m_OnColor = default;
    
    Color m_StartColor = default;
    Toggle m_Toggle;
 
    void OnEnable()
    {
        m_Toggle = GetComponent<Toggle>();
        m_Toggle.onValueChanged.AddListener(OnToggleValueChanged);

        m_StartColor = m_BorderImage.color;
    }

    void OnDisable()
    {
        m_Toggle.onValueChanged.RemoveListener(OnToggleValueChanged);
    }

    void OnToggleValueChanged(bool isOn)
    {
        m_BorderImage.color = isOn ? m_OnColor : m_StartColor;
    }
}
