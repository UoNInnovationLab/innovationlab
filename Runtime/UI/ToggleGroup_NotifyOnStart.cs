﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// A ToggleGroup doesn't send notification if a Toggle is already on.
/// Force the notification to be sent.
/// </summary>
///
[RequireComponent(typeof(ToggleGroup))]
public class ToggleGroup_NotifyOnStart : MonoBehaviour
{
    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        var toggleGroup = GetComponent<ToggleGroup>();

        if (toggleGroup.AnyTogglesOn())
        {
            // We need to flick the toggle off and then on.
            // If a toggle must be on, we need to inject a dummy toggle to switch to
            Toggle dummyToggle = null;
            if (!toggleGroup.allowSwitchOff)
            {
                dummyToggle = gameObject.AddComponent<Toggle>();
                toggleGroup.RegisterToggle(dummyToggle);
            }

            foreach (var toggle in toggleGroup.ActiveToggles())
            {
                if (dummyToggle != null)
                    dummyToggle.SetIsOnWithoutNotify(true);
                toggle.SetIsOnWithoutNotify(false);
                toggle.isOn = true;
            }

            if (dummyToggle)
            {
                toggleGroup.UnregisterToggle(dummyToggle);
                Destroy(dummyToggle);
            }
        }
    }
}