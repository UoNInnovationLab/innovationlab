﻿using System;
using UnityEngine;

public class VanishUi : MonoBehaviour
{
    private bool _scaledUp;
    
    private float _scaleTime = 0.5f;

    private float _startTime;
    
    private Vector3 _initScale;

    private Transform _toFollowTransform;

    public GameObject connectedTo;
    

    void Start()
    {
        _initScale = Vector3.one;//transform.localScale;
        transform.localScale = Vector3.zero;
        _scaledUp = false;
    }

    void OnDestroy()
    {
    }
   
    void FixedUpdate()
    {
        if(Camera.main != null)
            transform.LookAt(Camera.main.transform.position + (-Camera.main.transform.forward.normalized * 1f), Vector3.up);
        
        //if (Time.time < _startTime + _scaleTime)
        transform.localScale = Vector3.Slerp( _scaledUp ? Vector3.zero : _initScale, _scaledUp ? _initScale : Vector3.zero, (Time.time - _startTime) / _scaleTime);
            
        if(_toFollowTransform != null)
            transform.position = _toFollowTransform.position;
    }

    public void ScaleUpUi(float time)//, Transform toFollow)
    {
        _scaleTime = time;
        ScaleUpUi();
    }

    public void ScaleUpUi()
    {
        _scaledUp = true;
        _startTime = Time.time;
    }

    public void ScaleDownUi(float time)
    {
        _scaleTime = transform.localScale.sqrMagnitude <= 0.0001f ? 0f : time;
        ScaleDownUi();
    }

    public void ScaleDownUi()
    {
        _scaledUp = false;
        _startTime = Time.time;
    }

    public void SetUiFollow(Transform tran)
    {
        _toFollowTransform = tran;
    }

}
