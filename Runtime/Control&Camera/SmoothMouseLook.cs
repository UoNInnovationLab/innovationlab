﻿using UnityEngine;
using System.Collections.Generic;

public class SmoothMouseLook
{

    public enum RotationAxes
    {
        MouseXAndY = 0,
        MouseX = 1,
        MouseY = 2
    }

    public  RotationAxes axes = RotationAxes.MouseXAndY;
    public  float sensitivityX = 15F;
    public  float sensitivityY = 15F;

    public  float minimumX = -360F;
    public  float maximumX = 360F;

    public  float minimumY = -60F;
    public  float maximumY = 60F;

    private  float rotationX = 0F;
    private  float rotationY = 0F;

    private  List<float> rotArrayX = new List<float>();
    private  float rotAverageX = 0F;

    private  List<float> rotArrayY = new List<float>();
    private  float rotAverageY = 0F;

    public  float frameCounter = 5;
    
    public  void ClearArrays()
    {
        rotArrayX = new List<float>();
        rotArrayY = new List<float>();
    }
    
    public  Quaternion GetSmoothLook( Quaternion localRotation )
    {
        if (axes == RotationAxes.MouseXAndY)
        {
            rotAverageY = 0f;
            rotAverageX = 0f;
            
            rotationY += (Input.GetAxis("Mouse Y") * sensitivityY);
            rotationX += (Input.GetAxis("Mouse X") * sensitivityX);

            rotationY = ClampAngle(rotationY, minimumY, maximumY);
            rotationX = ClampAngle(rotationX, minimumX, maximumX);

            rotArrayY.Add(rotationY);
            rotArrayX.Add(rotationX);

            if (rotArrayY.Count >= frameCounter)
            {
                rotArrayY.RemoveAt(0);
            }
            if (rotArrayX.Count >= frameCounter)
            {
                rotArrayX.RemoveAt(0);
            }

            foreach (float t in rotArrayY)
                rotAverageY += t;

            foreach (float t in rotArrayX)
                rotAverageX += t;

            rotAverageY /= rotArrayY.Count;
            rotAverageX /= rotArrayX.Count;

            rotAverageY = ClampAngle(rotAverageY, minimumY, maximumY);
            rotAverageX = ClampAngle(rotAverageX, minimumX, maximumX);

            Quaternion yQuaternion = Quaternion.AngleAxis(rotAverageY, Vector3.left);
            Quaternion xQuaternion = Quaternion.AngleAxis(rotAverageX, Vector3.up);

            return localRotation * xQuaternion * yQuaternion;
        }
        else if (axes == RotationAxes.MouseX)
        {
            rotAverageX = 0f;

            rotationX += Input.GetAxis("Mouse X") * sensitivityX;

            rotArrayX.Add(rotationX);

            if (rotArrayX.Count >= frameCounter)
            {
                rotArrayX.RemoveAt(0);
            }
            foreach (float t in rotArrayX)
            {
                rotAverageX += t;
            }
            rotAverageX /= rotArrayX.Count;

            rotAverageX = ClampAngle(rotAverageX, minimumX, maximumX);

            Quaternion xQuaternion = Quaternion.AngleAxis(rotAverageX, Vector3.up);
            return localRotation * xQuaternion;
        }
        else
        {
            rotAverageY = 0f;

            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;

            rotArrayY.Add(rotationY);

            if (rotArrayY.Count >= frameCounter)
            {
                rotArrayY.RemoveAt(0);
            }
            for (int j = 0; j < rotArrayY.Count; j++)
            {
                rotAverageY += rotArrayY[j];
            }
            rotAverageY /= rotArrayY.Count;

            rotAverageY = ClampAngle(rotAverageY, minimumY, maximumY);

            Quaternion yQuaternion = Quaternion.AngleAxis(rotAverageY, Vector3.left);
            return localRotation * yQuaternion;
        }
    }

    public  float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }
            if (angle > 360F)
            {
                angle -= 360F;
            }
        }
        return Mathf.Clamp(angle, min, max);
    }
}