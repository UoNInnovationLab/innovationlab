using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// @shouvikme: Move anywhere
/// </summary>
public class FreeFlyingCamera : MonoBehaviour
{
    [Header("Speeds and sensitivities")]
    public float initialSpeed = 10f;
    public float increaseSpeed = 1.25f;
    
    public bool allowMovement = true;
    public bool allowRotation = true;

    [Header("Control Keys")]
    public KeyCode forwardButton = KeyCode.W;
    public KeyCode backwardButton = KeyCode.S;
    public KeyCode rightButton = KeyCode.D;
    public KeyCode leftButton = KeyCode.A;
    public KeyCode upButton = KeyCode.Q;
    public KeyCode downButton = KeyCode.E;

    [Header("Range Limits")]
    public float top, left, right, bottom;
    
    [Header("Enable Cursors")]
    public float cursorSensitivity = 0.025f;
    public bool cursorToggleAllowed = true;
    public KeyCode cursorToggleButton = KeyCode.Escape;

    private float _currentSpeed = 0f;
    private bool _moving = false;
    private bool _togglePressed = false;

    private bool _lockCamera = false;

    [Header("MenuOnCursorToggle")]
    public bool useMenuOnCursorToggle;

    [Header("Toggle list for menu enable")]
    public  List<GameObject> toSwitchOn;
    public  List<GameObject> toSwitchOff;

    private Quaternion initialRotation;

    readonly SmoothMouseLook _smoothMouseLook = new SmoothMouseLook
    {
        sensitivityX = 5f,
        sensitivityY = 5f,
        frameCounter = 10f
    };

    void Awake()
    {
    }

    void OnDestroy()
    {
    }

    private void Start()
    {
        initialRotation = transform.localRotation;
        _smoothMouseLook.ClearArrays();

        if (cursorToggleAllowed)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        
    }

    private void Update()
    {
        if (allowMovement && !_lockCamera)
        {
            var lastMoving = _moving;
            var deltaPosition = Vector3.zero;

            if (_moving)
                _currentSpeed += increaseSpeed * 0.8f * Time.deltaTime;

            _moving = false;

            CheckMove(forwardButton, ref deltaPosition, transform.forward);
            CheckMove(backwardButton, ref deltaPosition, -transform.forward);
            CheckMove(rightButton, ref deltaPosition, transform.right);
            CheckMove(leftButton, ref deltaPosition, -transform.right);
            CheckMove(upButton, ref deltaPosition, transform.up);
            CheckMove(downButton, ref deltaPosition, -transform.up);

            if (_moving)
            {
                if (_moving != lastMoving)
                    _currentSpeed = initialSpeed;
                var pos = transform.position + deltaPosition * _currentSpeed * Time.deltaTime;

                if (pos.y <= top && pos.y >= bottom && pos.z <= left && pos.z >= right && pos.x <= left &&
                    pos.x >= right)
                {
                    transform.position = pos;
                }
            }
            else
                _currentSpeed = 0f;
        }

        if (allowRotation && !_lockCamera)
            transform.localRotation = _smoothMouseLook.GetSmoothLook(initialRotation);

        if (cursorToggleAllowed)
        {
            if (Input.GetKey(cursorToggleButton))
            {
                if (!_togglePressed)
                {
                    _togglePressed = true;
                    ProcessCursorToggle();
                }
            }
            else
                _togglePressed = false;
        }
        else
        {
            _togglePressed = false;
            Cursor.visible = false;
        }
    }

    void OnApplicationFocus(bool hasFocus)
    {
        ProcessCursorToggle(!hasFocus);
    }
    void OnApplicationPause(bool pauseStatus)
    {
        ProcessCursorToggle(pauseStatus);
    }
    
    private void ProcessCursorToggle()
    {
        ProcessCursorToggle(!Cursor.visible);
    }

    private void ProcessCursorToggle(bool show)
    {
        Cursor.visible = show;

        _lockCamera = Cursor.visible;

        Cursor.lockState = _lockCamera ? CursorLockMode.Confined : CursorLockMode.Locked;

        _smoothMouseLook.ClearArrays();

        toSwitchOn.ForEach(o => o.SetActive(_lockCamera));
        toSwitchOff.ForEach(o => o.SetActive(!_lockCamera));

        if (useMenuOnCursorToggle)
        {
            //TODO attach menu window here to open & close on toggle
            /*
            if (_lockCamera)
                uiWindowMenuItem.Show();
            else
                uiWindowMenuItem.Hide();*/
        }

    }

    private void CheckMove(KeyCode keyCode, ref Vector3 deltaPosition, Vector3 directionVector)
    {
        if (Input.GetKey(keyCode))
        {
            _moving = true;
            deltaPosition += directionVector;
        }
    }
}
