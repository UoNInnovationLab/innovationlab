﻿using UnityEditor;
using UnityEngine;

namespace UnityToolbarExtender
{
    
    [InitializeOnLoad]
    public static class TimeScale
    {

        static TimeScale()
        {
            ToolbarExtender.LeftToolbarGUI.Add(OnToolbarGUI);
        }
        static void OnToolbarGUI()
        {
            Time.timeScale = EditorGUILayout.Slider(new GUIContent("", "Change the time scale"), Time.timeScale, 1, 20,GUILayout.Width(150));

        }
    }
}