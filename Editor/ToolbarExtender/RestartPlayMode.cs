﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnityToolbarExtender
{
    
    [InitializeOnLoad]
    public static class RestartPlayMode
    {

        static RestartPlayMode()
        {
            ToolbarExtender.RightToolbarGUI.Add(OnToolbarGUI);
        }
        static void OnToolbarGUI()
        {
            var tex = EditorGUIUtility.IconContent("RotateTool").image;

            GUI.changed = false;
            if (GUILayout.Button(new GUIContent(tex, "Restart Playmode"), "Command"))
            {
                if (EditorApplication.isPlaying)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            }

            
            //Time.timeScale = EditorGUILayout.Slider("", Time.timeScale, 1, 20,GUILayout.Width(150));

        }
    }
}