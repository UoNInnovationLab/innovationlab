﻿using UnityEditor;
using UnityEngine;

namespace UnityToolbarExtender
{
    
    [InitializeOnLoad]
    public static class DeletePlayerPrefs
    {

        static DeletePlayerPrefs()
        {
            ToolbarExtender.RightToolbarGUI.Add(OnToolbarGUI);
        }
        static void OnToolbarGUI()
        {
            var tex = EditorGUIUtility.IconContent(@"Grid.EraserTool").image;

            if (GUILayout.Button(new GUIContent(tex, "Clear player preferences"), "Command"))
            {
                PlayerPrefs.DeleteAll();
            }

        }
    }
}