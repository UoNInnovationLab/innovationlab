﻿namespace InnovationLab.Editor
{
    using UnityEngine;
    using UnityEditor;

    /// <summary>
    /// From http://wiki.unity3d.com/index.php/CreatePlane
    /// </summary>
    public class CreatePlane : ScriptableWizard
    {
        public enum Orientation
        {
            Horizontal,
            Vertical
        }

        public enum AnchorPoint
        {
            TopLeft,
            TopHalf,
            TopRight,
            RightHalf,
            BottomRight,
            BottomHalf,
            BottomLeft,
            LeftHalf,
            Center
        }

        public int m_WidthSegments = 1;
        public int m_LengthSegments = 1;
        public float m_Width = 1.0f;
        public float m_Length = 1.0f;
        public bool m_TileUV = false;
        public Orientation m_Orientation = Orientation.Horizontal;
        public AnchorPoint m_Anchor = AnchorPoint.Center;
        public bool m_AddCollider = false;
        public bool m_CreateAtOrigin = true;
        public bool m_TwoSided = false;
        public string m_OptionalName;

        static Camera m_Cam;
        static Camera m_LastUsedCam;


        [MenuItem("GameObject/Create Other/Custom Plane...")]
        static void CreateWizard()
        {
            m_Cam = Camera.current;
            // Hack because camera.current doesn't return editor camera if scene view doesn't have focus
            if (!m_Cam)
                m_Cam = m_LastUsedCam;
            else
                m_LastUsedCam = m_Cam;
            ScriptableWizard.DisplayWizard("Create Plane", typeof(CreatePlane));
        }


        void OnWizardUpdate()
        {
            m_WidthSegments = Mathf.Clamp(m_WidthSegments, 1, 254);
            m_LengthSegments = Mathf.Clamp(m_LengthSegments, 1, 254);
        }


        void OnWizardCreate()
        {
            GameObject plane = new GameObject();

            plane.name = !string.IsNullOrEmpty(m_OptionalName) ? m_OptionalName : "Plane";

            if (!m_CreateAtOrigin && m_Cam)
            {
                var transform = m_Cam.transform;
                plane.transform.position = transform.position + transform.forward * 5.0f;
            }
            else
                plane.transform.position = Vector3.zero;

            Vector2 anchorOffset;
            string anchorId;
            switch (m_Anchor)
            {
                case AnchorPoint.TopLeft:
                    anchorOffset = new Vector2(-m_Width / 2.0f, m_Length / 2.0f);
                    anchorId = "TL";
                    break;
                case AnchorPoint.TopHalf:
                    anchorOffset = new Vector2(0.0f, m_Length / 2.0f);
                    anchorId = "TH";
                    break;
                case AnchorPoint.TopRight:
                    anchorOffset = new Vector2(m_Width / 2.0f, m_Length / 2.0f);
                    anchorId = "TR";
                    break;
                case AnchorPoint.RightHalf:
                    anchorOffset = new Vector2(m_Width / 2.0f, 0.0f);
                    anchorId = "RH";
                    break;
                case AnchorPoint.BottomRight:
                    anchorOffset = new Vector2(m_Width / 2.0f, -m_Length / 2.0f);
                    anchorId = "BR";
                    break;
                case AnchorPoint.BottomHalf:
                    anchorOffset = new Vector2(0.0f, -m_Length / 2.0f);
                    anchorId = "BH";
                    break;
                case AnchorPoint.BottomLeft:
                    anchorOffset = new Vector2(-m_Width / 2.0f, -m_Length / 2.0f);
                    anchorId = "BL";
                    break;
                case AnchorPoint.LeftHalf:
                    anchorOffset = new Vector2(-m_Width / 2.0f, 0.0f);
                    anchorId = "LH";
                    break;
                case AnchorPoint.Center:
                // Fallthrough
                default:
                    anchorOffset = Vector2.zero;
                    anchorId = "C";
                    break;
            }

            MeshFilter meshFilter = (MeshFilter) plane.AddComponent(typeof(MeshFilter));
            plane.AddComponent(typeof(MeshRenderer));

            string planeAssetName =
                $"{plane.name}{m_WidthSegments}x{m_LengthSegments}W{m_Width}L{m_Length}{(m_Orientation == Orientation.Horizontal ? "H" : "V")}{anchorId}.asset";
            Mesh planeMesh = (Mesh) AssetDatabase.LoadAssetAtPath("Assets/Editor/" + planeAssetName, typeof(Mesh));

            if (planeMesh == null)
            {
                planeMesh = new Mesh {name = plane.name};

                int hCount2 = m_WidthSegments + 1;
                int vCount2 = m_LengthSegments + 1;
                int numTriangles = m_WidthSegments * m_LengthSegments * 6;
                if (m_TwoSided)
                {
                    numTriangles *= 2;
                }

                int numVertices = hCount2 * vCount2;

                Vector3[] vertices = new Vector3[numVertices];
                Vector2[] uvs = new Vector2[numVertices];
                int[] triangles = new int[numTriangles];
                Vector4[] tangents = new Vector4[numVertices];
                Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);

                int index = 0;
                float uvFactorX = m_TileUV ? m_Width / m_WidthSegments : 1.0f;
                float uvFactorY = m_TileUV ? m_Length / m_LengthSegments : 1.0f;
                float scaleX = m_Width / m_WidthSegments;
                float scaleY = m_Length / m_LengthSegments;
                for (float y = 0.0f; y < vCount2; y++)
                {
                    for (float x = 0.0f; x < hCount2; x++)
                    {
                        if (m_Orientation == Orientation.Horizontal)
                        {
                            vertices[index] = new Vector3(x * scaleX - m_Width / 2f - anchorOffset.x, 0.0f,
                                y * scaleY - m_Length / 2f - anchorOffset.y);
                        }
                        else
                        {
                            vertices[index] = new Vector3(x * scaleX - m_Width / 2f - anchorOffset.x,
                                y * scaleY - m_Length / 2f - anchorOffset.y, 0.0f);
                        }

                        tangents[index] = tangent;
                        uvs[index++] = new Vector2(x * uvFactorX, y * uvFactorY);
                    }
                }

                index = 0;
                for (int y = 0; y < m_LengthSegments; y++)
                {
                    for (int x = 0; x < m_WidthSegments; x++)
                    {
                        triangles[index] = (y * hCount2) + x;
                        triangles[index + 1] = ((y + 1) * hCount2) + x;
                        triangles[index + 2] = (y * hCount2) + x + 1;

                        triangles[index + 3] = ((y + 1) * hCount2) + x;
                        triangles[index + 4] = ((y + 1) * hCount2) + x + 1;
                        triangles[index + 5] = (y * hCount2) + x + 1;
                        index += 6;
                    }

                    if (m_TwoSided)
                    {
                        // Same tri vertices with order reversed, so normals point in the opposite direction
                        for (int x = 0; x < m_WidthSegments; x++)
                        {
                            triangles[index] = (y * hCount2) + x;
                            triangles[index + 1] = (y * hCount2) + x + 1;
                            triangles[index + 2] = ((y + 1) * hCount2) + x;

                            triangles[index + 3] = ((y + 1) * hCount2) + x;
                            triangles[index + 4] = (y * hCount2) + x + 1;
                            triangles[index + 5] = ((y + 1) * hCount2) + x + 1;
                            index += 6;
                        }
                    }
                }

                planeMesh.vertices = vertices;
                planeMesh.uv = uvs;
                planeMesh.triangles = triangles;
                planeMesh.tangents = tangents;
                planeMesh.RecalculateNormals();

                AssetDatabase.CreateAsset(planeMesh, $"Assets/{planeAssetName}");
                AssetDatabase.SaveAssets();
            }

            meshFilter.sharedMesh = planeMesh;
            planeMesh.RecalculateBounds();

            if (m_AddCollider)
                plane.AddComponent(typeof(BoxCollider));

            Selection.activeObject = plane;
        }
    }
}