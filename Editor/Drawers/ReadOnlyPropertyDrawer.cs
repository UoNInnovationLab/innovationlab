﻿using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace InnovationLab
{
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
    public class ReadOnlyPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(rect, label, property);

            GUI.enabled = false;
            EditorGUI.PropertyField(rect, property, label, true);
            GUI.enabled = true;

            EditorGUI.EndProperty();
        }
    }
}