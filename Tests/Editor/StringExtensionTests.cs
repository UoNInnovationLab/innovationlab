﻿using NUnit.Framework;

namespace InnovationLab.Tests.Editor
{
    public class StringExtensionTests
    {
        [Test]
        public void CapitalizeTest()
        {
            Assert.That("asdf".Capitalize(), Is.EqualTo("Asdf"));
            Assert.That("Asdf".Capitalize(), Is.EqualTo("Asdf"));
            Assert.That("".Capitalize(), Is.EqualTo(""));
            Assert.That("a".Capitalize(), Is.EqualTo("A"));
        }
        
        [Test]
        public void TrimStartTest()
        {
            Assert.That("asdf".TrimStart("a"), Is.EqualTo("sdf"));
            Assert.That("asdf".TrimStart("b"), Is.EqualTo("asdf"));
            Assert.That("asdf".TrimStart("asdf"), Is.EqualTo(""));
            Assert.That("".TrimStart("a"), Is.EqualTo(""));
        }
        
        
        
        [Test]
        public void IndexOfUppercaseTest()
        {
            Assert.That("".IndexOfUppercaseLetter(), Is.EqualTo(-1));
            Assert.That("Asdf".IndexOfUppercaseLetter(), Is.EqualTo(0));
            Assert.That("AsDf".IndexOfUppercaseLetter(1), Is.EqualTo(2));
        }

        [Test]
        public void IndexOfUpperLowerPairTest()
        {
            Assert.That("".IndexOfUpperLowerPair(), Is.EqualTo(-1));
            Assert.That("D".IndexOfUpperLowerPair(), Is.EqualTo(-1));
            Assert.That("Df".IndexOfUpperLowerPair(), Is.EqualTo(0));
            Assert.That("ASDfgH".IndexOfUpperLowerPair(), Is.EqualTo(2));
            Assert.That("gH".IndexOfUpperLowerPair(), Is.EqualTo(-1));
            Assert.That("OneTwo".IndexOfUpperLowerPair(2), Is.EqualTo(3));
        }

        [Test]
        public void SeparateWordsTest()
        {
            Assert.That("".SeparateWords(), Is.EqualTo(""));
            Assert.That("OneTwo".SeparateWords(), Is.EqualTo("One Two"));
            Assert.That("ASDfff".SeparateWords(), Is.EqualTo("AS Dfff"));
        }
    }
}